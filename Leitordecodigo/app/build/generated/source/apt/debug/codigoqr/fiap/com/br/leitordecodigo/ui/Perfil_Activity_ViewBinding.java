// Generated code from Butter Knife. Do not modify!
package codigoqr.fiap.com.br.leitordecodigo.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import codigoqr.fiap.com.br.leitordecodigo.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Perfil_Activity_ViewBinding implements Unbinder {
  private Perfil_Activity target;

  @UiThread
  public Perfil_Activity_ViewBinding(Perfil_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Perfil_Activity_ViewBinding(Perfil_Activity target, View source) {
    this.target = target;

    target.imgFoto = Utils.findRequiredViewAsType(source, R.id.img_foto, "field 'imgFoto'", ImageView.class);
    target.btnFoto = Utils.findRequiredViewAsType(source, R.id.botao_foto, "field 'btnFoto'", FloatingActionButton.class);
    target.edtLogin = Utils.findRequiredViewAsType(source, R.id.edtLogin, "field 'edtLogin'", EditText.class);
    target.edtSenha = Utils.findRequiredViewAsType(source, R.id.edtSenha, "field 'edtSenha'", EditText.class);
    target.edtNome = Utils.findRequiredViewAsType(source, R.id.edtNome, "field 'edtNome'", EditText.class);
    target.edtObjetivo = Utils.findRequiredViewAsType(source, R.id.edtObjetivo, "field 'edtObjetivo'", Spinner.class);
    target.btncadastrar = Utils.findRequiredViewAsType(source, R.id.btnCadastrar, "field 'btncadastrar'", Button.class);
    target.collapsingToolbarLayout = Utils.findRequiredViewAsType(source, R.id.collapsing_tolbar, "field 'collapsingToolbarLayout'", CollapsingToolbarLayout.class);
    target.toolbar = Utils.findRequiredView(source, R.id.tbl, "field 'toolbar'");
    target.appBarLayout = Utils.findRequiredViewAsType(source, R.id.app_bar_layout, "field 'appBarLayout'", AppBarLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Perfil_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.imgFoto = null;
    target.btnFoto = null;
    target.edtLogin = null;
    target.edtSenha = null;
    target.edtNome = null;
    target.edtObjetivo = null;
    target.btncadastrar = null;
    target.collapsingToolbarLayout = null;
    target.toolbar = null;
    target.appBarLayout = null;
  }
}
