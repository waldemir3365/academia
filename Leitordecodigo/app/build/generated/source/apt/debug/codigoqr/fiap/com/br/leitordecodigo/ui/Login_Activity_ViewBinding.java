// Generated code from Butter Knife. Do not modify!
package codigoqr.fiap.com.br.leitordecodigo.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import codigoqr.fiap.com.br.leitordecodigo.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Login_Activity_ViewBinding implements Unbinder {
  private Login_Activity target;

  @UiThread
  public Login_Activity_ViewBinding(Login_Activity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Login_Activity_ViewBinding(Login_Activity target, View source) {
    this.target = target;

    target.btnLogin = Utils.findRequiredViewAsType(source, R.id.btnLogin, "field 'btnLogin'", Button.class);
    target.edtUsuario = Utils.findRequiredViewAsType(source, R.id.edtUsuario, "field 'edtUsuario'", EditText.class);
    target.edtSenha = Utils.findRequiredViewAsType(source, R.id.edtSenha, "field 'edtSenha'", EditText.class);
    target.EsqueciSenha = Utils.findRequiredViewAsType(source, R.id.txt_esqueci_senha, "field 'EsqueciSenha'", TextView.class);
    target.inputLayout = Utils.findRequiredViewAsType(source, R.id.txt_input_layout, "field 'inputLayout'", TextInputLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Login_Activity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.btnLogin = null;
    target.edtUsuario = null;
    target.edtSenha = null;
    target.EsqueciSenha = null;
    target.inputLayout = null;
  }
}
