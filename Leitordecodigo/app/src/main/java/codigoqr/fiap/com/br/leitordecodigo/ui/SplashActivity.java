package codigoqr.fiap.com.br.leitordecodigo.ui;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import codigoqr.fiap.com.br.leitordecodigo.R;
import utils.NavigationUtils;

public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init(){
        loadSplash();
    }

    private void navigationLogin(){

        NavigationUtils.navigate(this, Login_Activity.class,true);
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void loadSplash(){

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                navigationLogin();
            }
        },3500);
    }
}
