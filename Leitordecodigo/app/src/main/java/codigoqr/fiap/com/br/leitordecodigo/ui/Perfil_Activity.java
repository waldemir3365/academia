package codigoqr.fiap.com.br.leitordecodigo.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import codigoqr.fiap.com.br.leitordecodigo.R;
import constants.ConstantsString;
import dao.AlunoDao;
import entity.AlunoBean;
import utils.CropImageUtils;
import utils.NavigationUtils;
import utils.ToastUtils;

public class Perfil_Activity extends AppCompatActivity {


    @BindView(value = R.id.img_foto)
    ImageView imgFoto;

    @BindView(value = R.id.botao_foto)
    FloatingActionButton btnFoto;

    @BindView(value = R.id.edtLogin)
    EditText edtLogin;

    @BindView(value = R.id.edtSenha)
    EditText edtSenha;

    @BindView(value = R.id.edtNome)
    EditText edtNome;

    @BindView(value = R.id.edtObjetivo)
    Spinner edtObjetivo;

    @BindView(value = R.id.btnCadastrar)
    Button btncadastrar;

    @BindView(value = R.id.collapsing_tolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(value = R.id.tbl)
    View toolbar;
    @BindView(value =R.id.app_bar_layout)
    AppBarLayout appBarLayout;


    private AlunoDao ad;
    private String login;
    private String senha;
    private String nome;
    private String objetivo;
    private File arquivoFoto = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_activity);


        ViewCompat.setTransitionName(findViewById(R.id.app_bar_layout), ConstantsString.EXTRA_IMAGE);

        //collapsingToolbarLayout.setTitle("item");
        //collapsingToolbarLayout.setExpandedTitleColor(Integer.valueOf(getColor(R.color.cor)));
//        btnFoto.setRippleColor(getResources().getColor(android.R.color.black));
      //  btnFoto.setBackgroundTintList(ColorStateList.valueOf(android.R.color.black));
        
        init();
    }

    private void init(){
        ButterKnife.bind(this);
        setValues();
    }

    private View.OnClickListener backButtonToobar() {
        return view -> {
            backHome();
        };
    }

    private void setValues(){

        ad = new AlunoDao(this);
        btncadastrar.setOnClickListener(adicionaAluno());
        btnFoto.setOnClickListener(photoOrGalery());
        setSettingsToobar();
        permissionCamera();
        permissionGalery();
    }

    private View.OnClickListener photoOrGalery() {
        return view ->{
            showDialogAddImage();
        };
    }

    private void showDialogAddImage(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(R.array.add_photo, onItemAddPhotoClickListener());
        builder.setTitle(ConstantsString.LOADIMAGE);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private DialogInterface.OnClickListener onItemAddPhotoClickListener() {

        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which){

                    case 0:
                        photoCamera();
                        break;
                    case 1:
                        photoGalery();
                        break;
                }

            }
        };
    }

    private void permissionCamera(){

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){


            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)){

            }else{

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        ConstantsString.PERMISSAO_REQUEST);
            }
        }
    }

    private void permissionGalery(){

        if (checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    ConstantsString.MY_CAMERA_REQUEST_CODE);
        }
    }

    private void photoCamera(){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(takePictureIntent.resolveActivity(getPackageManager()) != null){

            try {
                arquivoFoto = createFile();
            }catch (Exception ex){
                ToastUtils.toastLengthLong(this,ConstantsString.NOTCREATEIMAGE);
            }
        }

        if(arquivoFoto != null){

            Uri photoURI = FileProvider.getUriForFile(getBaseContext(), getBaseContext().getApplicationContext().getPackageName() +
                    ConstantsString.PROVIDER, arquivoFoto);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(takePictureIntent, ConstantsString.CAMERA);
        }
    }

    private void photoGalery(){

        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, ConstantsString.RESULT_LOAD_IMAGE);
    }

    private File createFile() {

        String timeStamp = new SimpleDateFormat(ConstantsString.PATTERNDATE).format(new Date());

        File pasta = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        File imagem = new File(pasta.getPath() + File.separator + ConstantsString.UNDERSCOREJPG
                + timeStamp + ConstantsString.PTJPG);

        return imagem;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ConstantsString.CAMERA && resultCode == RESULT_OK){

            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(arquivoFoto)));
            CropImageUtils.DoCrop((arquivoFoto),imgFoto);
        }

        if(requestCode == ConstantsString.RESULT_LOAD_IMAGE && resultCode == RESULT_OK){
            CropImageUtils.DoCrop(searchOnGalery(data), imgFoto);
        }
    }

    private File searchOnGalery(Intent data) {

        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String pathImagePhoto = cursor.getString(columnIndex);
        cursor.close();

        return  new File(pathImagePhoto);
    }

    private void setSettingsToobar() {

        setSupportActionBar((android.support.v7.widget.Toolbar) toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setTitle("Cadastrar Perfil");
        ((Toolbar) toolbar).setNavigationOnClickListener(backButtonToobar());
    }

    private void backHome()
    {
        NavigationUtils.navigate(this, Login_Activity.class, true);
        finish();
    }

    @Override
    public void onBackPressed() {
        backHome();
    }

    private View.OnClickListener adicionaAluno() {
        return view -> {
          recoveryAluno();
          insereAluno();
        };
    }

    private void recoveryAluno(){
        login = edtLogin.getText().toString();
        senha = edtSenha.getText().toString();
        nome = edtNome.getText().toString();
     //   objetivo = edtObjetivo.getText().toString();

    }
    private void insereAluno(){

        AlunoBean a = new AlunoBean(nome, login, senha, objetivo);
        ad.addAluno(a);
        ad.close();
        ToastUtils.toastLengthShort(Perfil_Activity.this, ConstantsString.ADDSUCESS);
        backHome();
    }
}
