package codigoqr.fiap.com.br.leitordecodigo.ui;


import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import codigoqr.fiap.com.br.leitordecodigo.R;
import constants.ConstantsString;
import dao.AlunoDao;
import entity.AlunoBean;
import utils.NavigationUtils;
import utils.ToastUtils;

import static constants.ConstantsString.USERNOTFOUND;


public class Login_Activity extends AppCompatActivity{

    @BindView(value = R.id.btnLogin)
    Button btnLogin;

    @NotEmpty(messageResId = R.string.validation_error_not_empty)
    @BindView(value = R.id.edtUsuario)
    EditText edtUsuario;

    @NotEmpty(messageResId = R.string.validation_error_not_empty)
    @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS, messageResId = R.string.validation_erlror_password)
    @BindView(value = R.id.edtSenha)
    EditText edtSenha;

    @BindView(value = R.id.txt_esqueci_senha)
    TextView EsqueciSenha;

    @BindView(value = R.id.txt_input_layout)
    TextInputLayout inputLayout;

    private AlunoDao ad;
    private String usuario;
    private String senha;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        init();
    }

    private void init(){

        ButterKnife.bind(this);
        setValues();
    }

    private void setValues()
    {
        EsqueciSenha.setOnClickListener(cadastraNewAluno());
        btnLogin.setOnClickListener(clickButtonEntrar());
        ad = new AlunoDao(this);
        validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {

                recoveryUser();
                sign();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {

                Iterator<ValidationError> iterator = errors.iterator();

                while (iterator.hasNext()) {

                    ValidationError error = iterator.next();
                    View view = error.getView();

                    if (view instanceof TextView) {
                        ((TextView) view).setError(error.getCollatedErrorMessage(Login_Activity.this));
                    }
                    iterator.remove();

                }
            }
        });
    }



    private View.OnClickListener clickButtonEntrar() {
        return view -> {
            validator.validate();
        };
    }

    private View.OnClickListener cadastraNewAluno() {
        return view ->{

            NavigationUtils.navigate(this, Perfil_Activity.class, true);
        };
    }

    private void recoveryUser(){

        usuario = edtUsuario.getText().toString();
        senha = edtSenha.getText().toString();
    }

    private void validateUserAndPassword(AlunoBean a){

        if(a != null)
            NavigationUtils.navigate(Login_Activity.this,Main2Activity.class,preencheBundle(a),true);
        else
            ToastUtils.toastLengthLong(Login_Activity.this, ConstantsString.USERNOTFOUND);
    }

    private Bundle preencheBundle(AlunoBean a){

        Bundle b = new Bundle();

        b.putString(a.NOME, a.getNome());
        b.putString(a.LOGIN,a.getLogin());
        b.putString(a.OBJETIVO,a.getObjetivo());
        b.putString(a.PAGAMENTO, a.getPagamento());

        return b;
    }

    private void sign() {

        SQLiteDatabase db = ad.getReadableDatabase();

        validateUserAndPassword(ad.getTudo(db, usuario, senha));
    }

}
