package constants;

public class ConstantsString {

    public static String USERNOTFOUND = "Usuario Não Encontrado !";
    public static String LOADIMAGE = "Carregar Imagem";
    public static String NOTCREATEIMAGE = "Não foi possível criar Imagem";
    public static String ADDSUCESS = "Cadastrado com Sucesso";


    public static String PROVIDER = ".provider";
    public static String PATTERNDATE = "yyyyMMdd_HHmmss";
    public static String PTJPG = ".jpg";
    public static String UNDERSCOREJPG = "JPG_";


    public static final int CAMERA = 9000;
    public static final int RESULT_LOAD_IMAGE = 9004;
    public static final int PERMISSAO_REQUEST = 2;
    public static final int MY_CAMERA_REQUEST_CODE = 100;
    public static final String EXTRA_IMAGE = "Tela Cadastro do Perfil";
}
