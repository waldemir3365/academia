package entity;

public class Endereco {

        private String cep;
        private String uf;
        private String bairro;
        private String cidade;
        private String logradouro;
        private String complemento;

    public Endereco(){}

    public Endereco(String cep, String uf, String bairro, String cidade, String logradouro, String complemento) {
        this.cep = cep;
        this.uf = uf;
        this.bairro = bairro;
        this.cidade = cidade;
        this.logradouro = logradouro;
        this.complemento = complemento;

    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    @Override
        public String toString() {
            return "Endereco{" +
                    "cep='" + cep + '\'' +
                    ", uf='" + uf + '\'' +
                    ", cidade='" + cidade + '\'' +
                    ", bairro='" + bairro + '\'' +
                    ", logradouro='" + logradouro + '\'' +
                    ", complemento='" + complemento + '\'' +
                    '}';
        }
}
