package utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.widget.ImageView;

import java.io.File;

public class CropImageUtils {

    public static void DoCrop(File arquivoFoto, ImageView imgFoto){

        int targetW = imgFoto.getWidth();
        int targetH = imgFoto.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(arquivoFoto.getAbsolutePath(), bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        Bitmap bitmap = BitmapFactory.decodeFile(arquivoFoto.getAbsolutePath(), bmOptions);

        setImage(bitmap,imgFoto);

    }

    private static void setImage(Bitmap bitmap, ImageView imgFoto){

        imgFoto.setImageBitmap(bitmap);
    }
}
