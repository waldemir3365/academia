package utils;

import android.app.Activity;
import android.widget.Toast;

public class ToastUtils {


    public static void toastLengthShort(Activity activity, String message){

        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public static void toastLengthLong(Activity activity, String message){

        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

}
