package utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class NavigationUtils {


    public static void navigate(Context context, Class<?> contextoDestino,  boolean clearStack){

        Intent i = new Intent(context, contextoDestino);

        if(clearStack) {

            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        }

        context.startActivity(i);
    }

    public static void navigate(Context context, Class clazz, Bundle bundle, boolean clearTop){

        Intent intent = new Intent(context, clazz);

        if(clearTop){
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }

        if(bundle != null){
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }


}
