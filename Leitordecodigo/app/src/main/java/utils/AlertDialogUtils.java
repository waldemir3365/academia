package utils;

import android.app.Activity;
import android.support.v7.app.AlertDialog;

public class AlertDialogUtils {

    public static void alertPadrao(Activity activity, String title, String message){

        new AlertDialog.Builder(activity)
                .setTitle(title)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(message)
                .show();
    }
}
